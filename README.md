# TalkInCode

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.1.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

## Notes from Alan Hill

I spent longer than I intended on this, however I could have spent more, so some notes to take into account:

1. I've used SCSS/css to style the pages to the approximate designs without spending needless time makin them pixel perfect.  I've designed it from mobile up, adding two break points for tablet (768) and desktop (1024).  Obviously for a production-strength solution I'd take much more time tweaking the styling so it was just right, and adding more breakpoints and slightly more elastic sizing between the breaks.

2. For speed I've used an NPM package that gave me the social share buttons, but it was limited in what you could customise for each one.  For a production ready version I'd create separate components for each of these to tailor the popups better according to the options given for each social media site.

3. I investigated unit testing and fixed a couple of issues I encountered with the standard tests added by the scaffolding code, however I don't have too much experience with automated unit testing, so didn't dwell on that too much for this test.

4. I've left out the social "counters" at the top - again they would require significant development effort, and since this is meant as a test, I figured they weren't the most important aspect of the solution.

The live URL is: https://test-in-code.herokuapp.com/

Within the zip file:

It's an Angular 2 project created using Angular CLI - so for reduction of the code I've removed the "node-modules" folder.  To run locally, create an empty folder, and run "npm install" to re-instate all the packages.  The "src" folder then contains all the raw typescript, javascript and scss, compiled during an "ng build" or "ng serve" step for testing locally.

The "dist" folder inside the zip contains the built solution ready for deployment.

As I decided to upload to Heroku (which expects a build process), I had to add an "index.php" file to the deployed code, so it's that which you visit when you go to the live site, all it does is include the "index.html" file.  

Finally, the respository is:

https://bitbucket.org/alanchill/talk-in-code



