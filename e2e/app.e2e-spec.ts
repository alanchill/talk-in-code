import { TalkInCodePage } from './app.po';

describe('talk-in-code App', () => {
  let page: TalkInCodePage;

  beforeEach(() => {
    page = new TalkInCodePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
