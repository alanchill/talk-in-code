import { Injectable } from '@angular/core';

@Injectable()
export class GlobalDataService {

  morseArray:Object;

  constructor() { 
    this.morseArray = {
      "A":".-",
      "B":"-...",
      "C":"-.-.",
      "D":"-..",
      "E":".",
      "F":"..-.",
      "G":"--.",
      "H":"....",
      "I":"..",
      "J":".---",
      "K":"-.-",
      "L":".-..",
      "M":"--",
      "N":"-.",
      "O":"---",
      "P":".--.",
      "Q":"--.-",
      "R":".-.",
      "S":"...",
      "T":"-",
      "U":"..-",
      "V":"...-",
      "W":".--",
      "X":"-..-",
      "Y":"-.--",
      "Z":"--..",
     " ":"/ "
}

  }



  getMorseArray(): Object {
    return this.morseArray;
  }

}
