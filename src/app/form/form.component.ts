import { Component, OnInit } from '@angular/core';
import { GlobalDataService } from '../global-data.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {ShareButtonsModule} from 'ngx-sharebuttons';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  providers: [GlobalDataService]
})
export class FormComponent implements OnInit {

  inputForm : FormGroup;

  selectedMessage: string;
  submitted: boolean = false;
  resultMorseCode: string;
  morseArray: Object;

  socialDesc: string;
  socialURL: string;

  constructor(private globalDataService: GlobalDataService, fb: FormBuilder) { 
    this.morseArray = globalDataService.morseArray;

    this.inputForm = fb.group({
      'message' : [null, [Validators.required, Validators.pattern(/^[a-zA-Z\s]+$/)]]
    })

    this.socialDesc = "";
    this.socialURL = "";
  }

  ngOnInit() {
  }

  submitForm(value: any):string{
    this.selectedMessage = value.message;
    this.submitted = true;
    this.resultMorseCode = "";

    for (var i = 0, len = value.message.length; i < len; i++) {
      // check if found in the global data
      //
      if(value.message[i].toUpperCase() in this.morseArray){
        this.resultMorseCode += this.morseArray[value.message[i].toUpperCase()];
      }
    }  

    this.socialURL = "https://test-in-code.herokuapp.com/";
    this.socialDesc = "Your code: " + this.resultMorseCode + " translate it at " + this.socialURL;

    return this.resultMorseCode;
  }

  reset(){
    this.selectedMessage = "";
    this.submitted = false;
    this.inputForm.reset();
  }

}
